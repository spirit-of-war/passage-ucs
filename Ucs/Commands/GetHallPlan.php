<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetHallPlan extends AbstractCommand
{
    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'GetHallPlan',
            'Sessions' => $params['SessionID'],     //обязательный
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
            'Levels' => '',
            'ListType' => 'Row;Place;X;Y;Width;Height;Type;Fragment;Object;Status;Busy;ModifiedType;NotAccessible;NotReservation;NotSale',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $hallLevelNodes = $xpath->query('/XML/Data/Session/Theatre/Hall/Levels/Level');
        foreach ($hallLevelNodes as $hallLevelNode) {
            $resultInfo = [];

            $objectNodes = $xpath->query('Objects/Object', $hallLevelNode);

            foreach ($objectNodes as $objectNode) {
                $object = [];
                $object['X'] = $objectNode->getAttribute('X');
                $object['Y'] = $objectNode->getAttribute('Y');
                $object['Width'] = $objectNode->getAttribute('Width');
                $object['Height'] = $objectNode->getAttribute('Height');
                $object['Name'] = $xpath->query('Name', $hallLevelNode)->item(0)->nodeValue;

                $objectColorNode = $xpath->query('Color', $hallLevelNode)->item(0);
                $object['R'] = $objectColorNode->getAttribute('R');
                $object['G'] = $objectColorNode->getAttribute('G');
                $object['B'] = $objectColorNode->getAttribute('B');

                $resultInfo['Objects'][] = $object;
            }

            $placesInfoNode = $xpath->query('Places', $hallLevelNode)->item(0);

            $resultInfo['PlacesCount'] = $placesInfoNode->getAttribute('PlacesCount');
            $resultInfo['PlacesLimit'] = $placesInfoNode->getAttribute('PlacesLimit');
            $resultInfo['Width'] = $placesInfoNode->getAttribute('Width');
            $resultInfo['Height'] = $placesInfoNode->getAttribute('Height');

            $placeNodes = $xpath->query('Places/Place', $hallLevelNode);

            foreach($placeNodes as $placeNode) {
                $place = [];
                $place['ID'] = $placeNode->getAttribute('ID');
                $place['Row'] = $placeNode->getAttribute('Row');
                $place['Place'] = $placeNode->getAttribute('Place');
                $place['X'] = $placeNode->getAttribute('X');
                $place['Y'] = $placeNode->getAttribute('Y');
                $place['Width'] = $placeNode->getAttribute('Width');
                $place['Height'] = $placeNode->getAttribute('Height');
                $place['Type'] = $placeNode->getAttribute('Type');
                $place['Fragment'] = $placeNode->getAttribute('Fragment');
                $place['Status'] = $placeNode->getAttribute('Status');
                $place['Reservation'] = $placeNode->getAttribute('Reservation');
                $place['Sale'] = $placeNode->getAttribute('Sale');
                $place['Level'] = $hallLevelNode->getAttribute('ID');

                $resultInfo['Places'][] = $place;
            }
            array_push($arr, $resultInfo);
        }

        return $arr;
    }
}