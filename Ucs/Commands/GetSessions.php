<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetSessions extends AbstractCommand
{
   public function execute(array $params)
   {
       $args = [
           'ServiceID' => $params['ServiceID'],
           'QueryCode' => 'GetSessions',
           'Movies' => $this->arrayToString($params['Movies']),
           'Theatres' => '',
           'Halls' => '',
           'DateList' => $this->arrayToString($params['DateList']),
           'ListSort' => 'CHLDM',
           'ListType' => 'BusyPlaces',
           'Encoding' => $params['Encoding'],
           'Archive' => 0,
           'Expect' => '',
           'Version' => $params['Version'],
       ];

       return parent::execute($args);
   }

   public function parseResponse($xpath)
   {
       $arr = [];

       $sessionNodes = $xpath->query('/XML/Data/Sessions/Session');
       foreach ($sessionNodes as $sessionNode) {
           $session = [];

           $theatreNode = $xpath->query('Theatre', $sessionNode)->item(0);
           $theatreId = $theatreNode->getAttribute('ID');

           $hallNode = $xpath->query('Hall', $theatreNode)->item(0);
           $hallId = $hallNode->getAttribute('ID');

           $movieNode = $xpath->query('Movie', $sessionNode)->item(0);
           $movieId = $movieNode->getAttribute('ID');

           $date = $xpath->query('Date', $sessionNode)->item(0)->nodeValue;
           $time = $xpath->query('Time', $sessionNode)->item(0)->nodeValue;

           $formatNode = $xpath->query('Format', $sessionNode)->item(0);
           $formatID = $formatNode->getAttribute('ID');
           $format = $formatNode->nodeValue;

           $sessionId = $sessionNode->getAttribute('ID');

           $arr['sessions'][$sessionId] = [
               'id' => $sessionId,
               'theatre' => [
                   'id' => $theatreId,
                   'hall' => [
                       'id' => $hallId,
                       'levels' => []
                   ]
               ],
               'movie' => [
                   'id' => $movieId,
               ],
               'date' => $date,
               'time' => $time,
               'format' =>[
                   'id' => $formatID,
                   'Text' => $format,
               ],

           ];
       }

       return $arr;
   }
}