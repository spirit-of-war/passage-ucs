<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetHalls extends AbstractCommand
{
    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'QueryCode' => 'GetHalls',
            'CardCode' => '',
            'DateList' => '',
            'Theatres' => '',
            'Encoding' => $params['Encoding'],
            'Version' => $params['Version'],
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $theatreNodes = $xpath->query('/XML/Data/Theatres/Theatre');
        foreach ($theatreNodes as $theatreNode) {
            $theatre = [];

            $theatreName = $xpath->query('Name', $theatreNode)->item(0)->nodeValue;

            $hallNodes = $xpath->query('Halls/Hall', $theatreNode);
            foreach ($hallNodes as $hallNode) {
                $hallId = $hallNode->getAttribute('ID');
                $hallName = $xpath->query('Name', $hallNode)->item(0)->nodeValue;

                $halls[$hallId] = [
                    'name' => $hallName,
                ];
            }
            $theatreId = $theatreNode->getAttribute('ID');

            $arr['theatres'][$theatreId] = [
                'name' => $theatreName,
                'halls' => $halls,
            ];
        }

        return $arr;
    }
}