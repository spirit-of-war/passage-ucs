<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class SaleApproved extends AbstractCommand
{
    public function execute(array $params)
    {
        if (!isset($params['CardCode'])){
            $params['CardCode'] = '770312293';
        }

        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'SaleApproved',
            'CardCode' => $params['CardCode'],              //обязательный
            'Sessions' => $params['SessionID'],             //обязательный
            'ReservationID' => $params['ReservationID'],    //обязательный
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];
        $resultInfo = [];

        $reservationNode = $xpath->query('/XML/Data/Reservation')->item(0);
        $resultInfo['ReservationId'] = $reservationNode->getAttribute('ID');

        $placeNodes = $xpath->query('Places/Place', $reservationNode);
        foreach($placeNodes as  $inx => $placeNode) {
            $resultInfo['Places'][$inx]['Level'] = $placeNode->getAttribute('Level');
            $resultInfo['Places'][$inx]['Fragment'] = $placeNode->getAttribute('Fragment');
            $resultInfo['Places'][$inx]['Row'] = $placeNode->getAttribute('Row');
            $resultInfo['Places'][$inx]['Place'] = $placeNode->getAttribute('Place');
            $resultInfo['Places'][$inx]['Code'] = $placeNode->getAttribute('Code');
        }

        array_push($arr, $resultInfo);


        return $arr;
    }
}