<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class Login extends AbstractCommand
{
    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'QueryCode' => 'Login',
            'CardCode' => $params['CardCode'],
            'CardPIN' => $params['CardPIN'],
            'Encoding' => $params['Encoding'],
            'Version' => $params['Version'],
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    protected function parseResponse($xpath)
    {
        $arr = [];

        $accountNodes = $xpath->query('/XML/Data/Accounts/Account');
        foreach ($accountNodes as $accountNode) {
            $accountName = $xpath->query('Name', $accountNode)->item(0)->nodeValue;

            $sumNode = $xpath->query('Balance', $accountNode)->item(0);
            $sum = $sumNode->getAttribute('Sum');
            $sumText = $sumNode->nodeValue;

            $arr['accounts'][$accountName] = [
                'name' => $accountName,
                'sum' => [
                    'value' => $sum / 100, // копейки в рубли
                    'text' => $sumText,
                ]
            ];
        }

        return $arr;
    }
}