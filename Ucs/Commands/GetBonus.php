<?php
// это тестовая ХАРДКОДная страница
namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetBonus extends AbstractCommand
{

    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'QueryCode' => 'Query_CardSystem',
            'QueryText' => '<?xml version="1.0" encoding="UTF-8"?><Message Action="Get card info" Terminal_Type="10" Global_Type="lUMkHdQJmXyQXhZ1TFyc" Unit_ID="1" User_ID="1"><Is_Virtual_Card>No</Is_Virtual_Card><Card_Code>' . $params['CardCode'] . '</Card_Code><Include>Holder,Account,Holder_Contact</Include></Message>',
            'Encoding'  => $params['Encoding'],
            'Version'   => $params['Version'],
            'Archive'   => 0,
            'Expect'    => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $cardNodes = $xpath->query('/XML/Data/Cards/Card');
        foreach ($cardNodes as $cardNode) {
            $card = $xpath->query('Card_Code', $cardNode)->item(0)->nodeValue;
            $arr['card'][]= $card;
        }



        $phoneNodes = $xpath->query('/XML/Data/Cards/Card/Holders_Contacts/Holder_Contact/Contact');
        foreach ($phoneNodes as $phoneNode) {
            $phone = $xpath->query('Value', $phoneNode)->item(0)->nodeValue;
            $arr['phones'][]= $phone;
        }

        $bonusNodes = $xpath->query('/XML/Data/Cards/Card/Accounts/Account');
        foreach ($bonusNodes as $bonusNode) {
            $type = mb_convert_encoding($xpath->query('Account_Type_Name', $bonusNode)->item(0)->nodeValue, "CP-1251");
            $balance = $xpath->query('Balance', $bonusNode)->item(0)->nodeValue;
            $arr['bonus'][$type][]= $balance;
        }

        return $arr;
    }
}