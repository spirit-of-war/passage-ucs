<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetMovies extends AbstractCommand
{
    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'QueryCode' => 'GetMovies',
            'Theatres' => '',
            'Halls' => '',
            'Movies' => $this->arrayToString($params['MoviesID']),
            'DateList' => $this->arrayToString($params['DateList']),
            'ListType' => 'PropertiesShow',
            'Encoding' => $params['Encoding'],
            'Version' => $params['Version'],
            'Expect' => '',
        ];
        $result = parent::execute($args);
        return $result;
    }
    public function parseResponse($xpath)
    {
        $arr = [];

        $movieNodes = $xpath->query('/XML/Data/Movies/Movie');

        foreach ($movieNodes as $movieNode) {
            $movieName = $xpath->query('Name', $movieNode)->item(0)->nodeValue;
            $movieID  = $movieNode->getAttribute('ID');

            $arr['Movies'][] = [
                'ID' => $movieID,
                'Name' => $movieName
            ];
        }

        return $arr;
    }
}