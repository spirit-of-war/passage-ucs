<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class Reservation extends AbstractCommand
{
    public function execute(array $params)
    {
        if (!isset($params['CardCode'])){
            $params['CardCode'] = '770312293';
        }

        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'Reservation',
            'CardCode' => $params['CardCode'],      //обязательный
            'Sessions' => $params['SessionID'],     //обязательный
            'Places' => $params['Places'],          //обязательный
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $reservationNodes = $xpath->query('/XML/Data/Reservation');
        foreach ($reservationNodes as $reservationNode) {
            $resultInfo = [];

            $resultInfo['ReservationId'] = $reservationNode->getAttribute('ID');

            $resultInfo['OwnerName'] = $xpath->query('Owner/Name', $reservationNode)->item(0)->nodeValue;
            $resultInfo['Sum'] = $xpath->query('Sum', $reservationNode)->item(0)->nodeValue;
            $resultInfo['Remark'] = $xpath->query('Remark', $reservationNode)->item(0)->nodeValue;

            array_push($arr, $resultInfo);
        }

        return $arr;
    }
}