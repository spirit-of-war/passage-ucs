<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetReservations extends AbstractCommand
{
    public function execute(array $params)
    {
        if (!isset($params['CardCode'])){
            $params['CardCode'] = '770312293';
        }

        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'GetReservations',
            'CardCode' => $params['CardCode'],      //обязательный
            'Sessions' => $params['SessionID'],
            'Places' => $params['Places'],
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
            'Customer' => $params['Customer'],
            'Movies' => $params['Movies'],
            'Theatres' => $params['Theatres'],
            'Halls' => $params['Halls'],
            'Levels' => $params['Levels'],
            'DateList' => $params['DateList'],
            'ListType' => $params['ListType'],

        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $reservationNodes = $xpath->query('/XML/Data/Reservations/Reservation');
        foreach ($reservationNodes as $reservationNode) {
            $resultInfo = [];

            $resultInfo['ReservationId'] = $reservationNode->getAttribute('ID');

            $resultInfo['OwnerName'] = $xpath->query('Owner/Name', $reservationNode)->item(0)->nodeValue;
            $resultInfo['Creation'] = $xpath->query('Creation', $reservationNode)->item(0)->nodeValue;

            $sessionNode = $xpath->query('Session', $reservationNode)->item(0);
            $resultInfo['SessionID'] = $sessionNode->getAttribute('ID');

            $theatreNode = $xpath->query('Theatre', $sessionNode)->item(0);
            $resultInfo['TheatreID'] = $theatreNode->getAttribute('ID');

            $resultInfo['MovieID'] = $xpath->query('Movie', $sessionNode)->item(0)->getAttribute('ID');
            $resultInfo['Date'] = $xpath->query('Date', $sessionNode)->item(0)->nodeValue;
            $resultInfo['Time'] = $xpath->query('Time', $sessionNode)->item(0)->nodeValue;

            $hallNode = $xpath->query('Hall', $theatreNode)->item(0);
            $resultInfo['HallID'] = $hallNode->getAttribute('ID');

            $levelNodes = $xpath->query('Levels/Level', $hallNode);
            foreach($levelNodes as $levelNode) {
                $levelID = $levelNode->getAttribute('ID');
                $resultInfo['Levels'][$levelID]['ID'] = $levelID;

                $placeNodes = $xpath->query('Places/Place', $levelNode);
                foreach($placeNodes as $placeNode) {
                    $placeInfo = [];
                    $placeInfo['Fragment'] = $placeNode->getAttribute('Fragment');
                    $placeInfo['Row'] = $placeNode->getAttribute('Row');
                    $placeInfo['Place'] = $placeNode->getAttribute('Place');
                    $placeInfo['Status'] = $placeNode->getAttribute('Status');

                    $resultInfo['Levels'][$levelID]['Places'][] = $placeInfo;

                }
            }

            array_push($arr, $resultInfo);
        }

        return $arr;
    }
}