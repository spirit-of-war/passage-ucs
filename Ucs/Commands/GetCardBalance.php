<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetCardBalance extends AbstractCommand
{
    protected $isLoginRequired = true;

    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'QueryCode' => 'GetCardBalance',
            'CardCode' => $params['CardCode'],
            'Encoding' => $params['Encoding'],
            'Version' => $params['Version'],
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $sumNodes = $xpath->query('/XML/Data/Sum');
        foreach ($sumNodes as $sumNode) {
            $sums = [];

            $sumType = $sumNode->getAttribute('Type');

            $arr['sums'][$sumType] = [
                'type' => $sumType,
                'name' => $sumNode->getAttribute('Name'),
                'sum' => $sumNode->getAttribute('Sum'),
            ];
        }

        return $arr;
    }
}