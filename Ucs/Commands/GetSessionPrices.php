<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetSessionPrices extends AbstractCommand
{
    public function execute(array $params)
    {
        $args = [
            'ServiceID' => $params['ServiceID'],
            'QueryCode' => 'GetSessionPrices',
            'Sessions' => $this->arrayToString($params['Sessions']),
            'Encoding' => $params['Encoding'],
            'Levels' => '',
            'Expect' => '',
            'Version' => $params['Version'],
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $placeTypeNodes = $xpath->query('/XML/Data/PlacesTypes/PlaceType');
        foreach ($placeTypeNodes as $placeTypeNode) {
            $placeType = [];

            $name = $xpath->query('Name', $placeTypeNode)->item(0)->nodeValue;

            $sumNode = $xpath->query('Sum', $placeTypeNode)->item(0);
            $sum = $sumNode->getAttribute('Sum');
            $sumText = $sumNode->nodeValue;

            $placeTypeId = $placeTypeNode->getAttribute('ID');

            $arr['placeTypes'][$placeTypeId] = [
                'id' => $placeTypeId,
                'name' => $name,
                'sum' => [
                    'value' => $sum / 100, // копейки в рубли
                    'text' => $sumText,
                ]
            ];
        }

        return $arr;
    }
}