<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class GetSales extends AbstractCommand
{
    public function execute(array $params)
    {
        if (!isset($params['CardCode'])){
            $params['CardCode'] = '770312293';
        }

        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'GetSales',
            'CardCode' => $params['CardCode'],      //обязательный
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
            'Sessions' => $params['SessionID'],
            'Movies' => $params['Movies'],
            'Theatres' => $params['Theatres'],
            'Halls' => $params['Halls'],
            'Levels' => $params['Levels'],
            'DateList' => $params['DateList'],
            'ListType' => $params['ListType'],
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $reservationNodes = $xpath->query('/XML/Data/Sales/Reservation');
        foreach ($reservationNodes as $reservationNode) {
            $resultInfo = [];

            $resultInfo['ReservationId'] = $reservationNode->getAttribute('ID');

            $resultInfo['Creation'] = $xpath->query('Creation', $reservationNode)->item(0)->nodeValue;
            $resultInfo['Expired'] = $xpath->query('Expired', $reservationNode)->item(0)->nodeValue;

            $sessionNode = $xpath->query('Session', $reservationNode)->item(0);
            $resultInfo['SessionID'] = $sessionNode->getAttribute('ID');

            $theatreNode = $xpath->query('Theatre', $sessionNode)->item(0);
            $resultInfo['TheatreID'] = $theatreNode->getAttribute('ID');

            $resultInfo['MovieID'] = $xpath->query('Movie', $sessionNode)->item(0)->getAttribute('ID');
            $resultInfo['Date'] = $xpath->query('Date', $sessionNode)->item(0)->nodeValue;
            $resultInfo['Time'] = $xpath->query('Time', $sessionNode)->item(0)->nodeValue;

            $hallNode = $xpath->query('Hall', $theatreNode)->item(0);
            $resultInfo['HallID'] = $hallNode->getAttribute('ID');

            $levelNodes = $xpath->query('Levels/Level', $hallNode);
            foreach($levelNodes as $levelNode) {
                $levelID = $levelNode->getAttribute('ID');
                $resultInfo['Levels'][$levelID]['ID'] = $levelID;
                $resultInfo['Levels'][$levelID]['Sum'] = $xpath->query('Sum', $levelNode)->item(0)->nodeValue;

                $placeNodes = $xpath->query('Places/Place', $levelNode);
                foreach($placeNodes as $placeNode) {
                    $placeInfo = [];
                    $placeInfo['Fragment'] = $placeNode->getAttribute('Fragment');
                    $placeInfo['Row'] = $placeNode->getAttribute('Row');
                    $placeInfo['Place'] = $placeNode->getAttribute('Place');
                    $placeInfo['Status'] = $placeNode->getAttribute('Status');

                    $resultInfo['Levels'][$levelID]['Places'][] = $placeInfo;

                }
            }

            array_push($arr, $resultInfo);
        }

        return $arr;
    }
}