<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class SaleReservation extends AbstractCommand
{
    public function execute(array $params)
    {
        if (!isset($params['CardCode'])){
            $params['CardCode'] = '770312293';
        }

        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'SaleReservation',
            'CardCode' => $params['CardCode'],                    //обязательный
            'Sessions' => $params['SessionID'],                   //обязательный
            'Places' => $params['Places'],                        //обязательный
            'ReservationID' => $params['ReservationID'],          //обязательный
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        $reservationNodes = $xpath->query('/XML/Data/Reservation');
        foreach ($reservationNodes as $reservationNode) {
            $resultInfo = [];

            $resultInfo['ReservationId'] = $reservationNode->getAttribute('ID');

            $resultInfo['Sum'] = $xpath->query('Sum', $reservationNode)->item(0)->nodeValue;
            $resultInfo['Expired'] = $xpath->query('Expired', $reservationNode)->item(0)->nodeValue;

            array_push($arr, $resultInfo);
        }

        return $arr;
    }
}