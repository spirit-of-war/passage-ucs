<?php

namespace Api\Ucs\Commands;

use Api\Ucs\AbstractCommand;

class ReservationClear extends AbstractCommand
{
    public function execute(array $params)
    {
        if (!isset($params['CardCode'])){
            $params['CardCode'] = '770312293';
        }

        $args = [
            'ServiceID' => $params['ServiceID'],
            'Version' => $params['Version'],
            'QueryCode' => 'ReservationClear',
            'CardCode' => $params['CardCode'],              //обязательный
            'Sessions' => $params['SessionID'],             //обязательный
            'ReservationID' => $params['ReservationID'],    //обязательный
            'Places' => $params['Places'],
            'Encoding' => $params['Encoding'],
            'Archive' => 0,
            'Expect' => '',
        ];

        return parent::execute($args);
    }

    public function parseResponse($xpath)
    {
        $arr = [];

        return $arr;
    }
}