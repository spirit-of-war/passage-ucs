<?php

namespace Api\Ucs;

class CommandFactory
{
    public static function getCommand($action)
    {
        $class = 'Api\\Ucs\\Commands\\' . ucfirst($action);

        if (!class_exists($class)) {
            throw new \Exception('Класс ' . $class . ' не найден');
        }

        $cmd = new $class();

        return $cmd;
    }
}