<?php

namespace Api\Ucs;

interface CommandInterface
{
    public function execute(array $params);
}