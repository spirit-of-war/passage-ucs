<?php

namespace Api\Ucs;

class Connector
{
    private $isLogged = false;

    private $socket;

    public function __construct($address, $port)
    {   

        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        socket_set_nonblock($this->socket);
        $time_socket_connect = time();

        while (!@socket_connect($this->socket, $address, $port)) {
            $err = socket_last_error($this->socket);

            if($err === 56) {
                // echo('connected ok');
                // echo $time_socket_connect - time();
                break;
            }

            if ((time() - $time_socket_connect) >= 3) {

                socket_close($this->socket);
                // echo('timeout reached!');
                break;
            }

            usleep(250000);
        }

        socket_set_block($this->socket);

        // socket_connect($this->socket, $address, $port);

        // if ($this->socket < 0) {
        //     throw new \Exception('socket_create() failed: '.socket_strerror(socket_last_error())."\n");
        // }

        // $result = socket_connect($this->socket, $address, $port);
        // if ($result === false) {
        //     throw new \Exception('socket_connect() failed: '.socket_strerror(socket_last_error())."\n");
        // }
    }

    public function write($command)
    {
        $len = strlen($command);

        // считаем степень числа
        $number = 10;
        for($row = 1; $number < $len; $row++) {
            $number = pow(10, $row+1);
        }

        // добавляем нули
        for ($i=$row; $i < 10; $i++) {
            $len = '0' . (string)$len;
        }
        $command = $len . '&' . $command;

        if ($this->isLogged) {
            echo 'command = '.$command.PHP_EOL;
        }

        try {
            socket_write($this->socket, $command, strlen($command));
            $data = socket_read($this->socket, 1024);
            // throw new \Exception(socket_strerror(socket_last_error()));
            // var_dump($data);
            $out = '';
            for (; $data != ''; ) {
                $out .= $data;
                $data = socket_read($this->socket, 1024);
            }
        } catch (\Exception $e) {
            echo "\nError: ".$e->getMessage().PHP_EOL;
        }

        if ($this->isLogged) {
            echo  'result = '.$out.PHP_EOL;
        }

        $xml = substr($out, strpos($out, '&') + 1);

        if ($this->isLogged) {
            echo  'xml = '.$xml.PHP_EOL;
        }

        return $xml;
    }

    public function __destruct()
    {
        // socket_close($this->socket);
    }
}