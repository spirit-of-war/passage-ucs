<?php

namespace Api\Ucs;

class Client
{
    private $address;
    private $port;
    private $serviceId;

    private $encoding = 'Windows-1251';
    private $version = '3';

    private $CardCode = '770312293';
    private $CardPIN = '2986';

    protected $connector = null;

    public function __construct($address, $port, $serviceId)
    {
        $this->address = $address;
        $this->port = $port;

        $this->serviceId = $serviceId;
    }

    public function run($action, array $params)
    {
        $command = CommandFactory::getCommand($action);
        $command->setConnector(new Connector($this->address, $this->port));

        $params['ServiceID'] = $this->serviceId;
        $params['Encoding'] = $this->encoding;
        $params['Version'] = $this->version;



        $res = $command->execute($params);
//        PRE($res);
        if ($res === false) {
            if (!isset($params['CardCode'])){
                $params['CardCode'] = $this->CardCode;
                $params['CardPIN'] = $this->CardPIN;
            }

            $login = CommandFactory::getCommand('Login');
            $login->setConnector(new Connector($this->address, $this->port));

            $olol = $login->execute($params);
//            PRE($olol);

            $command->setConnector(new Connector($this->address, $this->port));
            $res = $command->execute($params);
//            PRE($res);
        }

        return $res;
    }
}