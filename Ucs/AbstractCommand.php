<?php

namespace Api\Ucs;

abstract class AbstractCommand implements CommandInterface
{
    protected $connector;

    public function setConnector(Connector $connector)
    {
        $this->connector = $connector;
    }

    public function getConnector()
    {
        return $this->connector;
    }

    public function execute(array $params)
    {
        $xml = $this->connector->write(
            $this->httpBuildQuery($params)
        );

        $reg = '/<Data><!\[CDATA\[<\?xml.*?\?\>(.*?)\]\]><\/Data>/sim';

        $xml = preg_replace($reg, '<Data>\1</Data>', $xml);
//         var_dump($xml);

        $dom = new \DOMDocument;

        if (!$dom->loadXML($xml)) {
            return false;
        }

        $xpath = new \DOMXPath($dom);
        $result = $xpath->query('/XML/Result')->item(0)->nodeValue;

        if ($result == 'Ok') {
            $arr = $this->parseResponse($xpath);
        } else {
            $arr['code'] = $xpath->query('/XML/Error')->item(0)->nodeValue;
            $arr['message'] = $xpath->query('/XML/Remark')->item(0)->nodeValue;

            if ($arr['code'] == "CEE-010"){
                return false;
            }
        }
//        $test1 = $dom->saveXML();
//        $dom->save('test1.xml');
        return $this->postProcess($arr);
    }

    protected function postProcess($response)
    {
        return $response;
    }

    protected function arrayToString($arr)
    {
        if (!is_array($arr)) {
            return $arr;
        }

        return implode(';', $arr);
    }

    protected function httpBuildQuery(array $args)
    {
        $query = '';

        $i = 0;
        foreach ($args as $k => $v) {
            if ($i > 0) {
                $query .= '&'; // separator
            }
            $query .= $k . '=' . $v;

            $i++;
        }

        return $query;
    }
}